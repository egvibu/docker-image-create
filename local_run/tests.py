import time
import unittest
import xmlrunner
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import exceptions
from selenium.webdriver.common.by import By


class VdsinaEnter(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = None
        self.options = webdriver.ChromeOptions()
        self.options.add_argument("--start-maximized")
        self.options.add_argument("user-data-dir=session_files")

    def setUp(self):
        attempts, self.driver = 0, None
        while attempts < 10:
            try:
                self.driver = webdriver.Chrome('chromedriver', options=self.options)
                break
            except:
                self.driver = None
                attempts += 1
                time.sleep(2)
        if not self.driver:
            print("CAN'T CONNECT TO SELENIUM DOCKER CONTAINER")
            exit(-1)

    def tearDown(self):
        self.driver.quit()

    def test_entering_vdsina(self):

        self.driver.get("https://www.vdsina.ru")

        wait = WebDriverWait(self.driver, timeout=10, ignored_exceptions=[exceptions.NoSuchElementException]).until

        wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[class="auth__link js-pop-up-btn-open"]'))).click()
        time.sleep(1)

        login = wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="login"]')))
        login.send_keys(os.environ["CIVAR_VDSINA_LOGIN"])
        password = wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="passw"]')))
        password.send_keys(os.environ["CIVAR_VDSINA_PASSWORD"])
        password.send_keys(Keys.ENTER)

        time.sleep(5)

        self.driver.switch_to.window(self.driver.window_handles[1])

        time.sleep(5)

        assert "https://cp.vdsina.ru/vds/list" == self.driver.current_url


class VdsinaReenter(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = None
        self.options = webdriver.ChromeOptions()
        self.options.add_argument("--start-maximized")
        self.options.add_argument("user-data-dir=session_files")

    def setUp(self):
        attempts, self.driver = 0, None
        while attempts < 10:
            try:
                self.driver = webdriver.Chrome('chromedriver', options=self.options)
                break
            except:
                self.driver = None
                attempts += 1
                time.sleep(2)
        if not self.driver:
            print("CAN'T CONNECT TO SELENIUM DOCKER CONTAINER")
            exit(-1)

    def tearDown(self):
        self.driver.quit()

    def test_driver_alive(self):
        wait = WebDriverWait(self.driver, timeout=10, ignored_exceptions=[exceptions.NoSuchElementException]).until

        self.driver.get("https://www.vdsina.ru")
        wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[class="auth__link"]'))).click()
        time.sleep(2)
        self.driver.switch_to.window(self.driver.window_handles[1])

        assert "https://cp.vdsina.ru/vds/list" == self.driver.current_url


if __name__ == '__main__':
    with open('../report_selen.xml', 'w') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)
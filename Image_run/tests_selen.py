import time
import unittest
import xmlrunner
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import exceptions
from selenium.webdriver.common.by import By


options = webdriver.ChromeOptions()
capabilities = options.to_capabilities()
tries_count = 0

attempts, driver = 0, None
while attempts < 10:
    try:
        driver = webdriver.Remote( command_executor='http://chrome:4444/wd/hub', desired_capabilities=capabilities)
        break
    except:
        driver = None
        attempts += 1
        time.sleep(2)
if not driver:
    print("CAN'T CONNECT TO SELENIUM DOCKER CONTAINER")
    exit(-1)



class VdsinaEnter(unittest.TestCase):

    def test_entering_vdsina(self):

        driver.get("https://www.vdsina.ru")
        time.sleep(5)

        wait = WebDriverWait(driver, timeout=10, ignored_exceptions=[exceptions.NoSuchElementException]).until

        wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[class="auth__link js-pop-up-btn-open"]'))).click()
        login = wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="login"]')))
        login.send_keys(os.environ["CIVAR_VDSINA_LOGIN"])
        password = wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[name="passw"]')))
        password.send_keys(os.environ["CIVAR_VDSINA_PASSWORD"])
        driver.save_screenshot("screen1.png")
        password.send_keys(Keys.ENTER)
        time.sleep(5)
        #driver.save_screenshot("screen2.png")

        wait(EC.presence_of_element_located((By.CSS_SELECTOR, 'a[class="auth__link"]'))).click()
        time.sleep(5)

        driver.switch_to.window(driver.window_handles[1])

        with open('balance.txt', 'w') as fin:
            bal = driver.find_element(By.CSS_SELECTOR, '.info-box-balance-content span:nth-child(2)').text.split()
            b1 = ''
            if len(bal) > 1:
                for elem in bal:
                   b1 += elem
            fin.write(b1)
            fin.close()
        assert "https://cp.vdsina.ru/vds/list" == driver.current_url


    def test_driver_alive(self):
        assert "https://cp.vdsina.ru/vds/list" == driver.current_url


if __name__ == '__main__':
    with open('../report_selen.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)
import unittest
import xmlrunner
import json
import subprocess
import shlex
import os


class TestSinaAPI(unittest.TestCase):
    def setUp(self):
        self.string = 'curl -X GET -H "Authorization: ' + os.environ['CIVAR_VDSINA_TOKEN']

    def test_get_servers_list(self):
        args = shlex.split(self.string + '" "https://userapi.vdsina.ru/v1/server"')
        jsonData = subprocess.check_output(args)

        server_list = json.loads(jsonData)
        print(server_list['data'])
        assert server_list['data'][0]['name'] == "Egor-deploy"

    def test_get_balance(self):
        args = shlex.split(self.string + '" "https://userapi.vdsina.ru/v1/account.balance"')
        jsonData = subprocess.check_output(args)

        balance_list = json.loads(jsonData)
        with open('balance.txt', 'r') as check:
            assert float(balance_list['data']['real']) == float(check.read())
            check.close()

    def test_get_account_info(self):
        args = shlex.split(self.string + '" "https://userapi.vdsina.ru/v1/account"')
        jsonData = subprocess.check_output(args)

        info = json.loads(jsonData)
        assert info['data']['account']['name'] == 'a182042'


if __name__ == '__main__':
    with open('report_API.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)
# syntax=docker/dockerfile:1
FROM python:3.9.2

WORKDIR /root/test
COPY ./*.py ./
COPY ./requirements.txt ./

# CIVAR_VDSINA_LOGIN
# CIVAR_VDSINA_PASSWORD
# CIVAR_VDSINA_TOKEN

# Work dir: /root/test
# Tests output: /var/log/tests (use cp to put reports there)

RUN pip install -r requirements.txt

CMD ["bash",  "-c", "rm -rf /var/log/tests/*.xml; python tests_selen.py; python tests_rest.py;\
 junit2html report_API.xml report_API.html;\
 mkdir -p /var/log/tests; cp *.xml /var/log/tests; cp *.html /var/log/tests; cp *.png /var/log/tests"]